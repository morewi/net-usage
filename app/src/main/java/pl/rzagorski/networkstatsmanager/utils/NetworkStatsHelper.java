package pl.rzagorski.networkstatsmanager.utils;

import android.annotation.TargetApi;
import android.app.usage.NetworkStats;
import android.app.usage.NetworkStatsManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.RemoteException;
import android.telephony.TelephonyManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Robert Zagórski on 2016-09-09.
 */
@TargetApi(Build.VERSION_CODES.M)
public class NetworkStatsHelper {

    NetworkStatsManager networkStatsManager;
    int packageUid;


    SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
    String dateInString = "01-02-2017 00:00:00";
    Date date = sdf.parse(dateInString);
    DateAndCalendar obj = new DateAndCalendar();

    //2. Test - Convert Date to Calendar
    Calendar calendarStart = obj.dateToCalendar(date);

    String dateInString2 = "28-02-2017 23:59:00";
    Date date2 = sdf.parse(dateInString2);
    DateAndCalendar obj2 = new DateAndCalendar();

    //2. Test - Convert Date to Calendar
    Calendar calendarEnd = obj2.dateToCalendar(date2);



    public class DateAndCalendar {


        //Convert Date to Calendar
        private Calendar dateToCalendar(Date date) {

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            return calendar;

        }

        //Convert Calendar to Date
        private Date calendarToDate(Calendar calendar) {
            return calendar.getTime();
        }

    }

    public NetworkStatsHelper(NetworkStatsManager networkStatsManager) throws ParseException {
        this.networkStatsManager = networkStatsManager;
    }

    public NetworkStatsHelper(NetworkStatsManager networkStatsManager, int packageUid) throws ParseException {
        this.networkStatsManager = networkStatsManager;
        this.packageUid = packageUid;
    }

    public long getAllRxBytesMobile(Context context) {
        NetworkStats.Bucket bucket;
        try {
            bucket = networkStatsManager.querySummaryForDevice(ConnectivityManager.TYPE_MOBILE,
                    getSubscriberId(context, ConnectivityManager.TYPE_MOBILE),
                    0,
                    System.currentTimeMillis());
        } catch (RemoteException e) {
            return -1;
        }
        return bucket.getRxBytes();
    }

    public long getAllTxBytesMobile(Context context) {
        NetworkStats.Bucket bucket;
        try {
            bucket = networkStatsManager.querySummaryForDevice(ConnectivityManager.TYPE_MOBILE,
                    getSubscriberId(context, ConnectivityManager.TYPE_MOBILE),
                    0,
                    System.currentTimeMillis());
        } catch (RemoteException e) {
            return -1;
        }
        return bucket.getTxBytes();
    }

    public long getAllRxBytesWifi() {
        NetworkStats.Bucket bucket;
        try {
            bucket = networkStatsManager.querySummaryForDevice(ConnectivityManager.TYPE_WIFI,
                    "",
                    0,
                    System.currentTimeMillis());
        } catch (RemoteException e) {
            return -1;
        }
        return bucket.getRxBytes();
    }

    public long getAllTxBytesWifi() {
        NetworkStats.Bucket bucket;
        try {
            bucket = networkStatsManager.querySummaryForDevice(ConnectivityManager.TYPE_WIFI,
                    "",
                    0,
                    System.currentTimeMillis());
        } catch (RemoteException e) {
            return -1;
        }
        return bucket.getTxBytes();
    }

    public long getPackageRxBytesMobile(Context context) {
        NetworkStats networkStats = null;
        long startTime = calendarStart.getTimeInMillis();
        long endTime = calendarEnd.getTimeInMillis();

        /*
        Calendar calendar = Calendar.getInstance();
        long endTime = calendar.getTimeInMillis();
        calendar.add(Calendar.YEAR, -1);
        long startTime = calendar.getTimeInMillis();*/
        try {
            networkStats = networkStatsManager.queryDetailsForUid(
                    ConnectivityManager.TYPE_MOBILE,
                    getSubscriberId(context, ConnectivityManager.TYPE_MOBILE),
                    startTime,
                    endTime,
                    packageUid);
        } catch (RemoteException e) {
            return -1;
        }
        return getDataFromBucket(networkStats)[1];
    }

    public long getPackageTxBytesMobile(Context context) {
        NetworkStats networkStats = null;
        long startTime = calendarStart.getTimeInMillis();
        long endTime = calendarEnd.getTimeInMillis();
        try {
            networkStats = networkStatsManager.queryDetailsForUid(
                    ConnectivityManager.TYPE_MOBILE,
                    getSubscriberId(context, ConnectivityManager.TYPE_MOBILE),
                    startTime,
                    endTime,
                    packageUid);
        } catch (RemoteException e) {
            return -1;
        }
        return getDataFromBucket(networkStats)[0];
    }

    public long getPackageRxBytesWifi() {
        NetworkStats networkStats = null;
        Calendar calendar = Calendar.getInstance();
        long endTime = calendar.getTimeInMillis();
        calendar.add(Calendar.YEAR, -1);
        long startTime = calendar.getTimeInMillis();
        try {
            networkStats = networkStatsManager.queryDetailsForUid(
                    ConnectivityManager.TYPE_WIFI,
                    "",
                    startTime,
                    endTime,
                    packageUid);
        } catch (RemoteException e) {
            return -1;
        }
        return getDataFromBucket(networkStats)[1];
    }

    public static long[] getDataFromBucket(NetworkStats bucketForApp) {

        long dataTx = 0;
        long dataRx = 0;

        NetworkStats.Bucket bucket;

        while (bucketForApp.hasNextBucket()) {
            bucket = new NetworkStats.Bucket();
            bucketForApp.getNextBucket(bucket);
            dataTx += bucket.getTxBytes();
            dataRx += bucket.getRxBytes();

        }

        return new long[]{dataTx/1000/1000, dataRx/1000/1000};
    }

    public long getPackageTxBytesWifi() {
        NetworkStats networkStats = null;
        try {
            networkStats = networkStatsManager.queryDetailsForUid(
                    ConnectivityManager.TYPE_WIFI,
                    "",
                    0,
                    System.currentTimeMillis(),
                    packageUid);
        } catch (RemoteException e) {
            return -1;
        }
        return getDataFromBucket(networkStats)[0];
    }

    private String getSubscriberId(Context context, int networkType) {
        if (ConnectivityManager.TYPE_MOBILE == networkType) {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            return tm.getSubscriberId();
        }
        return "";
    }
}
